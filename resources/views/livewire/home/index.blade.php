<div>
  <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
  <div>
    <a href="https://api.whatsapp.com/send?phone=3117688000" id="btn-wp" class="btn">
      <img src="{{ asset('images/Whatsapp_icon-icons.com_66931.png') }}" alt="">
    </a>
  </div>
  <div class="container-fluid m-0 p-0">
    <div id="carouselExampleControls" class="carousel slide" data-bs-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img src="{{ asset('images/pexels-expect-best-323780 (1).jpg') }}" class="d-block w-100 img-fluid" alt="...">
        </div>
        <div class="carousel-item">
          <img src="{{ asset('images/pexels-frans-van-heerden-1438832.png') }}" class="d-block w-100 img-flui"
            alt="...">
        </div>
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleControls" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleControls"
        data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
  </div>
  <div class="container" style="background-color: rgb(232, 235, 235)">
    <div class="row d-flex justify-content-center mb-5" style="margin-top: 5%">
      <div class="col-2" style="margin-top: 4%">
        <h1>INMUEBLES</h1>
      </div>
    </div>
    <div class="row" style="margin-bottom: 10%">
      @foreach ($inmuebles as $key => $inmueble)
        <div class="col-4 d-flex justify-content-center mb-5">
          <div class="card card-noticias" style="width: 18rem; box-shadow: rgba(0, 0, 0, 0.35) 0px 5px 15px;)">
            @php
              $var = null;
              if (isset($inmuebles[$key]->imagenes)) {
                  $var = json_decode($inmuebles[$key]->imagenes);
              }
            @endphp
            <div id="carouselExampleControls{{ $key }}" class="carousel slide" data-bs-ride="carousel">
              <div class="carousel-inner">
                @if ($var)
                  @foreach ($var as $i => $imagen)
                    <div
                      @if ($i == 0) class="carousel-item active" @else class="carousel-item" @endif>
                      <img src="{{ url(str_replace('public', 'storage', $imagen)) }}" class="d-block w-100"
                        style="height: 250px">
                    </div>
                  @endforeach
                @else
                  <img src="{{ asset('images/pexels-pixabay-164558.jpg') }}" class="d-block w-100" alt="..."
                    style="height: 250px">
                @endif
              </div>
              @if (isset($var) && count($var) > 1)
                <button class="carousel-control-prev" type="button"
                  data-bs-target="#carouselExampleControls{{ $key }}" data-bs-slide="prev">
                  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button"
                  data-bs-target="#carouselExampleControls{{ $key }}" data-bs-slide="next">
                  <span class="carousel-control-next-icon" aria-hidden="true"></span>
                  <span class="visually-hidden">Next</span>
                </button>
              @endif
            </div>
            <div class="card-body">
              <h5 class="card-title">{{ $inmueble->direccion }}</h5>
              <p class="card-text">{{ $inmueble->tipo }}</p>
              <p class="card-text">{{ $inmueble->tipoNegocio }}</p>
              <p class="card-text">{{ $inmueble->valor }}$</p>
              <div class="d-flex justify-content-between">
                <div>
                  <a wire:click="$emit('abrirModalContacto')" class="btn btn-primary btn-sm">Solicitar</a>
                </div>
                <div class="d-flex align-items-end">
                  @switch($inmueble->estado)
                    @case('En gestión')
                      <span class="badge rounded-pill bg-secondary" id="badge-categoria">{{ $inmueble->estado }}</span>
                    @break

                    @case('Disponible')
                      <span class="badge rounded-pill bg-info" id="badge-categoria">{{ $inmueble->estado }}</span>
                    @break

                    @case('Vendida')
                      <span class="badge rounded-pill bg-danger" id="badge-categoria">{{ $inmueble->estado }}</span>
                    @break

                    @case('Arquilada')
                      <span class="badge rounded-pill bg-warning" id="badge-categoria">{{ $inmueble->estado }}</span>
                    @break
                  @endswitch
                </div>
              </div>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>

  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <livewire:contacto-inmueble />
      </div>
    </div>
  </div>
  @include('livewire.home.footer')
  <script>
    window.addEventListener('mdlCloseForm', (event) => {
      $('#staticBackdrop').modal('hide');
    });
    document.addEventListener('DOMContentLoaded', function() {
      window.livewire.on('alert', msg => {
        Swal.fire(
          'Good job!',
          'You clicked the button!',
          'success'
        )
      });
    });

    document.addEventListener('DOMContentLoaded', function() {
      window.livewire.on('abrirModalContacto', msg => {
        $('#staticBackdrop').modal('show');
      });
    });
    document.addEventListener('DOMContentLoaded', function() {
      window.livewire.on('modalNoticia', msg => {
        $('#crearNoticia').modal('show');
      });
    });
    window.addEventListener('cerrarModalNoticia', (event) => {
      $('#crearNoticia').modal('hide');
    });
  </script>
</div>
