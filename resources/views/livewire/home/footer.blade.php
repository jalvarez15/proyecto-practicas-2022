<div class="container-fluid  pt-5 pb-5" style="min-height: 150px; background: rgb(11, 22, 77)">
  <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
  <div class="row d-flex justify-content-center align-items-center">
    <div class="col-1 d-flex justify-content-center align-items-center">
      <a href="{{ route('home') }}">
        <x-jet-application-mark class="block h-9 w-auto" />
      </a>
    </div>
    <div class="col-2 d-flex justify-content-center align-items-center">

      <ul>
        <h5 class="h5-footer">COMPAÑIA</h5>
        <li>
          <a class="a-footer" href="{{ route('home') }}">Acerca</a>
        </li>
        <li>
          <a class="a-footer" href="{{ route('gestion') }}">Misión</a>
        </li>
        <li>
          <a class="a-footer" href="{{ route('noticias') }}">Visión</a>
        </li>
        <li>
          <a class="a-footer" href="{{ route('noticias') }}">Equipo</a>
        </li>
      </ul>
    </div>
    <div class="col-2 d-flex justify-content-center align-items-center">
      <ul>
        <h5 class="h5-footer">INMUEBLES</h5>
        <li>
          <a class="a-footer" href="{{ route('home') }}">Casas</a>
        </li>
        <li>
          <a class="a-footer" href="{{ route('gestion') }}">Apartamentos</a>
        </li>
        <li>
          <a class="a-footer" href="{{ route('noticias') }}">Estudios</a>
        </li>
        <li>
          <a class="a-footer" href="{{ route('noticias') }}">Bodegas</a>
        </li>
      </ul>
    </div>
    <div class="col-2 d-flex justify-content-center align-items-center">
      <ul>
        <h5 class="h5-footer">INFORMACIÓN</h5>
        <li>
          <a class="a-footer" href="{{ route('home') }}">Terminos y condiciones</a>
        </li>
        <li>
          <a class="a-footer" href="{{ route('gestion') }}">Politicas</a>
        </li>
        <li>
          <a class="a-footer" href="{{ route('noticias') }}">Seguridad</a>
        </li>
        <li>
          <a class="a-footer" href="{{ route('noticias') }}">Datos personales</a>
        </li>
      </ul>
    </div>
    <div class="col-3">
      <div class="row  d-flex justify-content-center align-items-center">
        <div class="col-12" style="text-align: center">
          <h5 class="mb-4 border-none" style="color: white">SIGUENOS</h5>
        </div>
        <div class="col-2">
          <a class="a-footer" href=""><img src="{{ asset('images/facebook_108044.png') }}" alt=""
              class="social"></a>
        </div>
        <div class="col-2">
          <a class="a-footer" href=""><img src="{{ asset('images/instagram_108043.png') }}" alt=""
              class="social"></a>
        </div>
        <div class="col-2">
          <a class="a-footer" href=""><img src="{{ asset('images/twitter_socialnetwork_20007.png') }}" alt=""
              class="social"></a>
        </div>
        <div class="col-2">
          <a class="a-footer" href=""><img src="{{ asset('images/linkedin_icon-icons.com_65929.png') }}" alt=""
              class="social"></a>
        </div>
        <div class="col-2">
          <a class="a-footer" href=""><img src="{{ asset('images/telegram_icon-icons.com_72055.png') }}" alt=""
              class="social"></a>
        </div>
      </div>
    </div>
  </div>
</div>
