<div>
  <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Crear noticia</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
  </div>
  <div class="modal-body">
    <div class="container">
      <form wire:submit.prevent="save">
        <div class="row">
          <div class="col-6">
            <label>Titulo</label>
            <input type="text" class="form-control" wire:model="noticia.titulo">
          </div>
          <div class="col-6">
            <label>Categoria</label>
            <input type="text" class="form-control" wire:model="noticia.categoria">
          </div>
          <div class="col-12 mt-3">
            <div class="form-floating">
              <textarea class="form-control" placeholder="Leave a comment here" id="floatingTextarea"
                wire:model="noticia.descripcion" style="height: 150px"></textarea>
              <label for="floatingTextarea">Contenido de la noticia</label>
            </div>
          </div>
          <div class="col-12 d-flex justify-content-center mt-3">
            <div wire:loading wire:target="img">
              <div class="d-flex justify-content-center">
                <div class="spinner-border" role="status">
                  <span class="visually-hidden"></span>
                </div>
              </div>
            </div>
            @if ($img)
              @if ($noticia->imagen && $noticia->imagen == $img)
                <img src="{{ url(str_replace('public', 'storage', $noticia->imagen)) }}" width="150" height="150" class="img-fluid mb-3">
              @else
              <img width="150" height="150" class="img-noticia img-fluid" src="{{ $img->temporaryUrl() }}">
              @endif
            @else
              <img width="150" height="150" class="img-noticia img-fluid"
                src="{{ asset('images/pexels-josh-sorenson-1714208.jpg') }}">
            @endif
          </div>
          <div class="col-12 mb-3">
            <div class="input-group">
              <input type="text" class="form-control browse-file" placeholder="Choose" readonly>
              <label class="input-group-btn">
                <span class="btn btn-primary">
                  Browse <input wire:model="img" type="file" style="display: none;">
                </span>
              </label>
            </div>
          </div>
          <div class="col-12 d-flex justify-content-center">
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div>
        </div>
      </form>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
  </div>
</div>
