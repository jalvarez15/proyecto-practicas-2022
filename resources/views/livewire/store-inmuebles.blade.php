<div>
  <div class="container">
    <div class="card mt-5">
      <div class="card-body">
        <h5 class="card-title">
          <div class="row justify-content-between">
            <div class="col-6">
              <h1>Inmuebles</h1>
            </div>
            <div class="col-2">
              <button class="btn btn-outline-primary" type="button" class="btn btn-primary"
                wire:click="$emit('crearInmueble')">Agregar</button>
            </div>
          </div>
        </h5>
        <div class="row">
          <div class="col-12 table-responsive" style="height: 250px;">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Direccion</th>
                  <th scope="col">Valor</th>
                  <th scope="col">Estado</th>
                  <th scope="col">Tipo</th>
                  <th scope="col">Tipo de negocio</th>
                  <th scope="col">Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($inmuebles as $inmueble)
                  <tr>
                    <th>{{ $inmueble->direccion }}</th>
                    <td>{{ $inmueble->valor }}</td>
                    <td>{{ $inmueble->estado }}</td>
                    <td>{{ $inmueble->tipo }}</td>
                    <td>{{ $inmueble->tipoNegocio }}</td>
                    <td>
                      <button class="btn btn-outline-success"
                        wire:click="$emit('updateInmueble', {{ $inmueble->id }})">Edit</button>
                      <button class="btn btn-outline-danger"
                        wire:click="$emit('alertWarning', 'Seguro que quieres eliminar este inmueble?', 'inmueble', {{ $inmueble->id }})">Delet</button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <hr>
    <div class="card">
      <div class="card-body">
        <h5 class="card-title">
          <div class="row justify-content-between">
            <div class="col-6">
              <h1>Noticias</h1>
            </div>
            <div class="col-2">
              <button class="btn btn-outline-primary" type="button" class="btn btn-primary"
                wire:click="$emit('crearNoticia')">Agregar</button>
            </div>
          </div>
        </h5>
        <div class="row">
          <div class="col-12 table-responsive" style="height: 250px;">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col">Titulo</th>
                  <th scope="col">Categoria</th>
                  <th scope="col">Contenido</th>
                  <th scope="col">Imagen</th>
                  <th scope="col">Acción</th>
                </tr>
              </thead>
              <tbody>
                @foreach ($noticias as $noticia)
                  <tr>
                    <th>{{ $noticia->titulo }}</th>
                    <td>{{ $noticia->categoria }}</td>
                    <td>
                      <textarea readonly class="form-control" id="floatingTextarea" placeholder="{{ $noticia->descripcion }}"
                        wire:model="noticia.descripcion"></textarea>
                    </td>
                    <td>
                      @if (isset($noticia->imagen))
                        <img src="{{ url(str_replace('public', 'storage', $noticia->imagen)) }}"
                          class="img-fluid" width="150" height="150">
                      @else
                        <img width="150" height="150" class="img-noticia img-fluid"
                          src="{{ asset('images/pexels-josh-sorenson-1714208.jpg') }}">
                      @endif
                    </td>
                    <td>
                      <button class="btn btn-outline-success"
                        wire:click="$emit('editarNoticia', {{ $noticia->id }})">Edit</button>
                      <button class="btn btn-outline-danger"
                        wire:click="$emit('alertWarning', 'Seguro que quieres eliminar esta noticia?', 'noticia', {{ $noticia->id }})">Delet</button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
    <hr>
    @can('crearUsers')
    <div class="card">
        <div class="card-body">
          <h5 class="card-title">
            <div class="row justify-content-between">
              <div class="col-6">
                <h1>Usuarios</h1>
              </div>
              <div class="col-2">
                <button class="btn btn-outline-primary" type="button" class="btn btn-primary"
                  wire:click="$emit('crearUser')">Agregar</button>
              </div>
            </div>
          </h5>

          <div class="row">
            <div class="col-12 table-responsive" style="height: 250px;">
              <table class="table table-hover">
                <thead>
                  <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Nombre</th>
                    <th scope="col">Email</th>
                    <th scope="col">Rol</th>
                    <th scope="col">Acción</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach ($users as $user)
                    <tr>
                      <th>{{ $user->id }}</th>
                      <td>{{ $user->name }}</td>
                      <td>{{ $user->email }}</td>
                      <td>
                        @switch($user->role_id)
                          @case(1)
                            Administrador
                          @break

                          @case(2)
                            Asesor
                          @break

                          @case(3)
                            Usuario
                          @break

                          @default
                        @endswitch
                      </td>
                      <td>
                        <button class="btn btn-outline-success"
                          wire:click="$emit('updateUser', {{ $user->id }})">Edit</button>
                        <button class="btn btn-outline-danger"
                          wire:click="$emit('alertWarning', 'Seguro que quieres eliminar este usuario?', 'usuario', {{ $user->id }})">Delet</button>
                      </td>
                    </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    @endcan



  </div>

  <!-- Modal -->
  <div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <livewire:guardar-inmueble />
      </div>
    </div>
  </div>
  <div class="modal fade" id="crearNoticia" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <livewire:home.crear-noticia />
      </div>
    </div>
  </div>
  <div class="modal fade" id="crearUser" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
    aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <livewire:crear-user />
      </div>
    </div>
  </div>
  <script>
    window.addEventListener('mdlCloseForm', (event) => {
      $('#staticBackdrop').modal('hide');
    });
    document.addEventListener('DOMContentLoaded', function() {
      window.livewire.on('alert', msg => {
        Swal.fire({
          position: 'center',
          icon: 'success',
          title: 'Acción exitosa',
          showConfirmButton: false,
          timer: 1500
        })
      });
    });


    document.addEventListener('DOMContentLoaded', function() {
      window.livewire.on('alertWarning', (msg, elemento, id) => {
        Swal.fire({
          icon: 'warning',
          title: msg,
          showDenyButton: true,
          showCancelButton: false,
          confirmButtonText: 'Eliminar',
          denyButtonText: `Cancelar`,
        }).then((result) => {
          if (result.isConfirmed) {
            Swal.fire({
              position: 'center',
              icon: 'success',
              title: 'Acción exitosa',
              showConfirmButton: false,
              timer: 1500
            });
            if (elemento == 'usuario') {
              livewire.emit('deleteUser', id);
            } else if (elemento == 'inmueble') {
              livewire.emit('deleteInmueble', id);
            } else if (elemento == 'noticia') {
              livewire.emit('borrarNoticia', id);
            }
          } else if (result.isDenied) {
            Swal.fire({
              position: 'center',
              icon: 'error',
              title: 'No se elimino nada!',
              showConfirmButton: false,
              timer: 1200
            })
          }
        })
      });
    });

    document.addEventListener('DOMContentLoaded', function() {
      window.livewire.on('abrirModal', msg => {
        $('#staticBackdrop').modal('show');
      });
    });
    document.addEventListener('DOMContentLoaded', function() {
      window.livewire.on('modalNoticia', msg => {
        $('#crearNoticia').modal('show');
      });
    });
    window.addEventListener('cerrarModalNoticia', (event) => {
      $('#crearNoticia').modal('hide');
    });
    document.addEventListener('DOMContentLoaded', function() {
      window.livewire.on('modalUser', msg => {
        $('#crearUser').modal('show');
      });
    });
    window.addEventListener('cerrarModalUser', (event) => {
      $('#crearUser').modal('hide');
    });
  </script>
</div>
