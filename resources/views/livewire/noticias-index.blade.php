<div>
  <link rel="stylesheet" href="{{ asset('css/styles.css') }}">
  @if ($modo == 'index')
    <div class="container mt-5" style="background: rgb(236, 234, 234)">
      <div class="row d-flex justify-content-center">
        <div class="col-12 mt-5 mb-5 bg-light" style="text-align: center">
          <h1>NOTICIAS</h1>
        </div>
        @foreach ($noticias as $noticia)
          <div class="col-8" style="margin-bottom: 10%">
            <div class="container">
              <div class="card text-center card-noticias">
                @if (isset($noticia->imagen))
                  <img src="{{ url(str_replace('public', 'storage', $noticia->imagen)) }}"
                    class="img-noticia img-fluid">
                @else
                  <img class="img-noticia img-fluid" src="{{ asset('images/pexels-josh-sorenson-1714208.jpg') }}">
                @endif
                <div class="card-body">
                  <h5 class="card-title">{{ $noticia->titulo }}</h5>
                  @php
                    $var = substr($noticia->descripcion, 0, 300);
                  @endphp
                  <p class="card-text">{{ $var }}...</p>
                  <div class="row d-flex justify-content-between">
                    <div class="col-4">
                      <a href="#" wire:click="detalle({{ $noticia->id }})" class="btn btn-primary">Leer mas</a
                        href="#">
                    </div>
                    <div class="col-2 d-flex align-items-center">
                      <span class="badge rounded-pill bg-dark" id="badge-categoria">{{ $noticia->categoria }}</span>
                    </div>
                  </div>
                </div>
                <div class="card-footer text-muted">
                  Publicado: {{ $noticia->created_at->format('Y-M-d H:i:s') }}
                </div>
              </div>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  @else
    <div class="container">
      <div class="row d-flex justify-content-center">
        <div class="col-12 mt-5 mb-5" style="text-align: center">
          <button class="btn btn-dark mb-5" wire:click="$set('modo','index')" style="width: auto">Volver</button>
          <h1>{{ $noticia->titulo }}</h1>
        </div>
        <div class="col-8" style="margin-bottom: 10%">
          <div class="container">
            <div class="card text-center">
              @if (isset($noticia->imagen))
                <img src="{{ url(str_replace('public', 'storage', $noticia->imagen)) }}"
                  class="img-noticia img-fluid">
              @else
                <img class="img-noticia img-fluid" src="{{ asset('images/pexels-josh-sorenson-1714208.jpg') }}">
              @endif
              <div class="card-body">
                <p class="card-text">{{ $noticia->descripcion }}</p>
                <div class="row d-flex justify-content-end">
                  <div class="col-2 d-flex align-items-start">
                    <span class="badge rounded-pill bg-dark"
                      id="badge-categoria">{{ $noticia->categoria }}</span>
                  </div>
                </div>
              </div>
              <div class="card-footer text-muted">
                Publicado: {{ $noticia->created_at->format('Y-M-d H:i:s') }}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  @endif
  @include('livewire.home.footer')
</div>
