<div>
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Agregar Inmueble</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
  </div>
  <div class="modal-body">
    <div class="container">
      <form wire:submit.prevent="save">
        <div class="row">
          <div class="col-6 pb-3">
            <input class="form-control" wire:model="obj.direccion" type="text" placeholder="Dirección de inmueble">
          </div>
          <div class="col-6 pb-3">
            <input class="form-control" wire:model="obj.valor" type="number" placeholder="Valor">
          </div>
          <div class="col-6 pb-3">
            <select wire:model="obj.estado" class="form-control form-select form-select mb-3"
              aria-label=".form-select-lg example">
              <option selected>Estado</option>
              <option value="En gestión">En gestión</option>
              <option value="Disponible">Disponible</option>
              <option value="Vendida">Vendida</option>
              <option value="Arquilada">Arquilada</option>
            </select>
          </div>
          <div class="col-6 pb-3">
            <input class="form-control" wire:model="obj.tipo" type="text" placeholder="Apartamento/casa...">
          </div>
          <div class="col-6 pb-3">
            <input class="form-control" wire:model="obj.tipoNegocio" type="text" placeholder="Venta o arriendo">
          </div>
          <div class="col-6 pb-3">
            <input wire:model="photos" class="form-control" type="file" id="formFileMultiple" multiple>
          </div>
          @if ($photos)
            @if ($this->obj->imagenes)
              @php
                $var = json_decode($this->obj->imagenes);
              @endphp
              @if ($photos == $var)
                @foreach ($photos as $photo)
                  <div class="col-3 pb-3">
                    <img src="{{ url(str_replace('public', 'storage', $photo)) }}"
                      style="width: 100px; height: 100px">
                  </div>
                @endforeach
              @else
                @foreach ($photos as $photo)
                  <div class="col-3 pb-3">
                    <img src="{{ $photo->temporaryUrl() }}" style="width: 100px; height: 100px">
                  </div>
                @endforeach
              @endif
            @else
              @foreach ($photos as $photo)
                <div class="col-3 pb-3">
                  <img src="{{ $photo->temporaryUrl() }}" style="width: 100px; height: 100px">
                </div>
              @endforeach
            @endif
          @else
            <div class="col-3 pb-3">
              <img src="{{ asset('images/pexels-pixabay-164558.jpg') }}" class="d-block w-100"
                style="width: 100px; height: 100px">
            </div>
          @endif
        </div>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </form>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
  </div>
</div>
