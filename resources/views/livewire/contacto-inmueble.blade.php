<div>
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Solicitar información sobre el inmueble</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
  </div>
  <div class="modal-body">
    <div class="container">
      <form wire:submit.prevent="enviar">
        <div class="row">
          <div class="col-6 pb-3">
            <input class="form-control" wire:model="email" type="text" placeholder="Dirección de correo">
          </div>
          <div class="col-6 pb-3">
            <input class="form-control" wire:model="telefono" type="number" placeholder="Numero">
          </div>
          <div class="col-12 pb-3">
            <div class="alert alert-primary" role="alert">
                Se enviara la información a un asesor y proximamente se comunicará con tigo!
            </div>
          </div>
        </div>
        <button type="submit" class="btn btn-primary">Enviar</button>
      </form>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
  </div>
</div>
