<div>
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Crear usuario</h5>
    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
  </div>
  <div class="modal-body">

    <div class="container">
      <div class="row">
        <form wire:submit.prevent="save">
          <div class="col-12">
            <div class="alert alert-warning" role="alert">
              El usuario tendrá permisos en el sistema!
            </div>
            <div class="form-floating mb-3">
              <input wire:model="name" required type="text" class="form-control" id="floatingName" placeholder="Nombre completo">
              <label for="floatingName">Nombre completo</label>
              @error('name') <span class="error" style="color: red">{{ $message }}</span> @enderror
            </div>
            <div class="form-floating mb-3">
              <input wire:model="email" required type="email" class="form-control" id="floatingInput" placeholder="name@example.com">
              <label for="floatingInput">Correo</label>
              @error('email') <span class="error" style="color: red">{{ $message }}</span> @enderror
            </div>
            <div class="form-floating">
              <input wire:model="password" type="text" class="form-control" id="floatingPassword" placeholder="Contraseña">
              <label for="floatingPassword">Contraseña</label>
              @if ($modo == 'editar')
              <div class="alert alert-secondary mt-3 mb-3" role="alert">
                <small>Por razones de seguridad la contraseña no se muestra</small>
              </div>
              @endif
              @error('password') <span class="error" style="color: red">{{ $message }}</span> @enderror
            </div>
            <select wire:model="role_id" class="form-select form-select mb-3 mt-3" aria-label=".form-select-lg example">
              <option selected>Selecciona el rol</option>
              <option value="1">Administrador</option>
              <option value="2">Asesor</option>
            </select>
          </div>
          <button type="submit" class="btn btn-primary">Confirmar</button>
        </form>
      </div>
    </div>
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
  </div>
</div>
