<?php

use App\Http\Livewire\Home\Index;
use App\Http\Livewire\NoticiasIndex;
use App\Http\Livewire\StoreInmuebles;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', Index::class)->name('home');
Route::get('noticias', NoticiasIndex::class)->name('noticias');

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified'
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    Route::get('gestion', StoreInmuebles::class)->name('gestion');

});
