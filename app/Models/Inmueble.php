<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inmueble extends Model
{
    protected $fillable = ['direccion','valor','estado','tipo','tipoNegocio','imagenes'];
    use HasFactory;
}
