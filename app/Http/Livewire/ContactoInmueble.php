<?php

namespace App\Http\Livewire;

use Illuminate\Support\Facades\Mail;
use Livewire\Component;

class ContactoInmueble extends Component
{
    public  $email, $telefono;
    protected $rules = [
        'email' => 'required',
        'telefono' => 'required'
    ];

    public function render()
    {
        return view('livewire.contacto-inmueble');
    }

    public function enviar(){

        //$this->validate();
        //$subject = "Asunto del correo";
        //$for = "correo_que_recibira_el_mensaje@gmail.com";
        //Mail::send('email',$request->all(), function($msj) use($subject,$for){
        //    $msj->from("tucorreo@gmail.com","NombreQueApareceráComoEmisor");
        //    $msj->subject($subject);
        //    $msj->to($for);
        //});

    }
}
