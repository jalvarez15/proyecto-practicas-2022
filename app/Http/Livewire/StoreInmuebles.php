<?php

namespace App\Http\Livewire;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\Inmueble;
use App\Models\Noticia;
use App\Models\User;
use Livewire\Component;

class StoreInmuebles extends Component
{
    use AuthorizesRequests;

    public $inmuebles, $noticias, $users;
    protected $listeners = ['render'];

    public function mount(){
        $this->authorize('gestion');
    }

    public function render()
    {
        $this->inmuebles = Inmueble::all();
        $this->noticias = Noticia::all();
        $this->users = User::all();
        return view('livewire.store-inmuebles');
    }
}
