<?php

namespace App\Http\Livewire;

use Livewire\WithFileUploads;

use App\Models\Inmueble;
use Livewire\Component;

class GuardarInmueble extends Component
{
    use WithFileUploads;
    public $obj, $photos = [];
    protected $listeners = ['updateInmueble', 'deleteInmueble', 'crearInmueble', 'render'];
    protected $rules = [
        'obj.direccion' => 'required',
        'obj.valor' => 'required',
        'obj.estado' => 'required',
        'obj.tipo' => 'required',
        'obj.tipoNegocio' => 'required'
    ];

    public function mount()
    {
        $this->obj = new Inmueble();
    }

    public function render()
    {
        return view('livewire.guardar-inmueble');
    }

    public function save()
    {
        $this->validate();
        if ($this->photos && $this->photos !== json_decode($this->obj->imagenes)) {
            $obj = [];
            foreach ($this->photos as $key => $photo) {
                $var = $photo->store('public/fotos');
                $obj[$key] = $var;
            }
            $this->obj->imagenes = json_encode($obj);
        }
        $this->obj->save();
        $this->dispatchBrowserEvent('mdlCloseForm');
        $this->emit('alert');
        $this->emit('render');
        $this->render();
    }

    public function crearInmueble()
    {
        $this->reset();
        $this->obj = new Inmueble();
        $this->emit('abrirModal');
    }

    public function updateInmueble($id)
    {
        $this->reset();
        $this->obj = Inmueble::find($id);
        if (isset($this->obj->imagenes)) {
            $this->photos = json_decode($this->obj->imagenes);
        }

        $this->emit('abrirModal');
    }

    public function deleteInmueble($id)
    {
        $this->obj = Inmueble::find($id);
        $this->obj->delete();
        $this->emit('render');
    }
}
