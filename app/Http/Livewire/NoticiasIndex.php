<?php

namespace App\Http\Livewire;

use App\Models\Noticia;
use Livewire\Component;

class NoticiasIndex extends Component
{
    public $noticias, $noticia, $modo = 'index';

    public function render()
    {
        $this->noticias = Noticia::all();
        return view('livewire.noticias-index');
    }

    public function detalle($id)
    {
        $this->noticia = Noticia::find($id);
        $this->modo = 'detalle';
    }
}
