<?php

namespace App\Http\Livewire\Home;

use App\Models\Noticia;
use Livewire\Component;
use Livewire\WithFileUploads;

class CrearNoticia extends Component
{
    use WithFileUploads;
    public $noticia, $img;
    protected $listeners = ['crearNoticia', 'editarNoticia', 'borrarNoticia'];
    protected $rules = [
        'noticia.titulo' => 'required',
        'noticia.descripcion' => 'required',
        'noticia.categoria' => 'required',
        'noticia.imagen' => 'nullable'
    ];

    public function render()
    {
        return view('livewire.home.crear-noticia');
    }

    public function crearNoticia()
    {
        $this->reset();
        $this->noticia = new Noticia();
        $this->emit('modalNoticia');
    }

    public function save()
    {
        $this->validate();

        if ($this->img && $this->img != $this->noticia->imagen) {
            $this->noticia->imagen = $this->img->store('public/fotos');;
        }
        $this->noticia->save();
        $this->dispatchBrowserEvent('cerrarModalNoticia');
        $this->emit('alert');
        $this->emit('render');
        $this->render();
    }

    public function editarNoticia($id)
    {
        $this->noticia = Noticia::find($id);
        if(isset($this->noticia->imagen)){
            $this->img = $this->noticia->imagen;
        }
        $this->emit('modalNoticia');
    }
    public function borrarNoticia($id)
    {
        $this->noticia = Noticia::find($id);
        $this->noticia->delete();
        $this->emit('render');
    }
}
