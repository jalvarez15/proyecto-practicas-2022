<?php

namespace App\Http\Livewire\Home;

use App\Models\Inmueble;
use Livewire\Component;

class Index extends Component
{
    public $inmuebles;
    public function render()
    {
        $this->inmuebles = Inmueble::all();
        return view('livewire.home.index');
    }
}
