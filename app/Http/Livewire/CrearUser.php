<?php

namespace App\Http\Livewire;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\User;
use Illuminate\Validation\Rule;
use Livewire\Component;

class CrearUser extends Component
{
    use AuthorizesRequests;
    public $user, $name, $email, $password, $role_id, $modo = '';
    protected $listeners = ['crearUser','updateUser','deleteUser'];

    public function rules(){
        return [
            'name' => 'required|min:10',
            'email' => ['required','email', Rule::unique('users', 'email')->ignore($this->user->id)],
            'role_id' => 'required',
            'password' => 'required',
        ];
    }

    protected $messages = [
        'email.required' => 'Es necesario una dirección de correo',
        'email.email' => 'El formato de correo no es valido',
        'email.unique' => 'El email no está disponible',
        'name.required' => 'El nombre completo es requerido',
        'name.min' => 'El nombre debe contener almenos 10 caracteres',
        'password.required' => 'La contraseña es requerida',
    ];

    public function mount(){
        $this->authorize('crearUsers');
    }

    public function render()
    {
        return view('livewire.crear-user');
    }

    public function updateUser($id)
    {
        $this->user = User::find($id);
        $this->name = $this->user->name;
        $this->email = $this->user->email;
        $this->password = '';
        $this->role_id = $this->user->role_id;
        $this->modo = 'editar';
        $this->emit('modalUser');
    }

    public function deleteUser($id)
    {
        $this->user = User::find($id);
        $this->user->delete();
        $this->emit('render');
    }

    public function crearUser()
    {
        $this->reset();
        $this->emit('modalUser');
        $this->modo = 'crear';
        $this->user = new User();
    }

    public function save()
    {
        $this->rules();
        $rol = '';
        if($this->role_id == 1){
            $rol = 'Admin';
        }else{
            $rol = 'Asesor';
        }
        $this->user->name = $this->name;
        $this->user->email = $this->email;
        $this->user->password = bcrypt($this->password);
        $this->user->role_id = $this->role_id;
        $this->user->save();
        $this->user->syncRoles($rol);

        $this->reset();
        $this->dispatchBrowserEvent('cerrarModalUser');
        $this->emit('alert');
        $this->emit('render');
    }

    public function updated($propertyName)
    {
        $this->validateOnly($propertyName);
    }
}
