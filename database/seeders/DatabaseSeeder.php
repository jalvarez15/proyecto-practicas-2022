<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{

    public function run()
    {
        $this->call(RoleSeeder::class);
        $this->call(InmuebleSeeder::class);
        $this->call(NoticiaSeeder::class);
        $this->call(UserSeeder::class);
    }
}
