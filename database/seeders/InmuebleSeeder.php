<?php

namespace Database\Seeders;

use App\Models\Inmueble;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class InmuebleSeeder extends Seeder
{

    public function run()
    {
        Inmueble::create([
            'direccion' => 'Calle 68 # 43-32',
            'valor' => 68000000,
            'estado' => 'En gestión',
            'tipo' => 'Casa',
            'tipoNegocio' => 'Venta'
        ]);
        Inmueble::create([
            'direccion' => 'Carrera 50 # 34-92',
            'valor' => 670000,
            'estado' => 'Disponible',
            'tipo' => 'Apartamento',
            'tipoNegocio' => 'Arquiler'
        ]);
        Inmueble::create([
            'direccion' => 'Calle 79 # 12-54',
            'valor' => 13000000,
            'estado' => 'Vendida',
            'tipo' => 'Casa',
            'tipoNegocio' => 'Venta'
        ]);
        Inmueble::create([
            'direccion' => 'Calle 80 # 13-84',
            'valor' => 15660000,
            'estado' => 'Arquilada',
            'tipo' => 'Casa',
            'tipoNegocio' => 'Arquiler'
        ]);
    }
}
