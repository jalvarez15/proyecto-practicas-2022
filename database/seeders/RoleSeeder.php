<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role1 = Role::create(['name' => 'Admin']);
        $role2 = Role::create(['name' => 'Asesor']);
        $role3 = Role::create(['name' => 'User']);

        Permission::create(['name' => 'gestion'])->syncRoles($role1, $role2);
        Permission::create(['name' => 'noticias'])->syncRoles($role1, $role2, $role3);
        Permission::create(['name' => 'crearUsers'])->syncRoles($role1);
    }
}
