<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{

    public function run()
    {
        User::create([
            'name' => 'David',
            'email' => 'demo@demo.com',
            'role_id' => 1,
            'password' => bcrypt('12345678')
        ])->assignRole('Admin');
        User::create([
            'name' => 'Alex',
            'email' => 'alex@demo.com',
            'role_id' => 2,
            'password' => bcrypt('12345678')
        ])->assignRole('Asesor');
    }
}
