<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{

    public function up()
    {
        Schema::create('inmuebles', function (Blueprint $table) {
            $table->id();
            $table->string('direccion');
            $table->string('tipo');
            $table->string('tipoNegocio');
            $table->unsignedBigInteger('valor');
            $table->string('estado');
            $table->json('imagenes')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('inmuebles');
    }
};
